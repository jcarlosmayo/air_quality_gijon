Air quality in Gijon
====================

The City of [Gijon](http://www.gijon.eu/documentos/departamentos/ma/estaciones/index.html) provides access to the city's [air quality measurements](http://datos.gijon.es/page/1808-catalogo-de-datos). The data currently available contains measurements taken from five weather stations on 17 variables, hourly, from 2000 until today.

The data can be downloaded in csv files containing the observations of a single year, which can then be combined into a larger data set. The description of the variables measured and their respective unit of measurement can be found, in Spanish, [here](http://opendata.gijon.es/descargar.php?id=5&tipo=HTML).


```r
library(ggplot2)
library(gridExtra)
```

```
## Loading required package: grid
```

```r
library(openair)
library(reshape2)
library(dplyr)
```

```
## 
## Attaching package: 'dplyr'
## 
## The following objects are masked from 'package:stats':
## 
##     filter, lag
## 
## The following objects are masked from 'package:base':
## 
##     intersect, setdiff, setequal, union
```

```r

# Set the working directory
setwd("~/Cloud Drive/Online Education/Exploratory Data Analysis/air_quality_gijon/data")

# --------------------------------------------------- Reading the data from
# csv files - One file per year
# ---------------------------------------------------

# Emtpy data frame to collect the whole data set
air <- data.frame()

# Empty data frame to collect information on valid observations per variable
# and year
valid_obs <- data.frame()


for (i in 2009:2013) {
    file <- paste(as.character(i), ".csv", sep = "")
    data <- read.csv(file, sep = ";")
    names(data) <- tolower(names(data))
    valid_pcnts <- c()
    
    # Extract the percentage of valid observations per variable to a vector
    for (i in 6:22) {
        names <- names(data)[i]
        nas <- sum(is.na(data[, i]))
        vobs <- nrow(data) - nas
        percent <- round(vobs/nrow(data), 2)
        valid_pcnts <- c(valid_pcnts, percent)
    }
    
    # Append the vector with the percentage of valid observations to the data
    # frame
    valid_obs <- rbind(valid_obs, c(valid_pcnts))
    
    # Append the data set to the data frame
    air <- rbind(air, data)
    
}

# Remove redundant variables created when loading the data
rm(file, data, names, nas, percent, valid_pcnts, vobs, i)

# Add the variable names and row names to the table with the percentage of
# valid observations
colnames(valid_obs) = names(air[6:22])
valid_obs$year <- c(2009:2013)

# ----------------------- Cleaning the data frame -----------------------

# Variables 'estaci.n', 'latitud' and 'longitud' are redundant
air <- subset(air, select = c(-estaci.n, -latitud, -longitud))

# Variable 't.tulo' --> 'station'
names(air)[1] = c("station")

# Rename the station level names 'Estaci\363n Avenida Argentina' -->
# 'argentina' 'Estaci\363n Avenida Castilla' --> 'castilla' 'Estaci\363n
# Avenida Constituci\363n' --> 'constitucion' 'Estaci\363n Avenida
# Hermanos Felgueroso' --> 'felgueroso' 'Estaci\363n de Montevil' -->
# 'montevil'
levels(air$station)[1] = "argentina"
levels(air$station)[2] = "castilla"
levels(air$station)[3] = "constitucion"
levels(air$station)[4] = "felgueroso"
levels(air$station)[5] = "montevil"

# Keep only measurements made at 'constitucion'
air <- subset(air, station == "constitucion")
air <- subset(air, select = -station)

# Convert the date and time to 'POSIXlt' 'POSIXt'
air$date <- strptime(air$fecha.solar..utc., "%Y-%m-%dT%H:%M:%S", tz = "GMT")
air <- subset(air, select = -fecha.solar..utc.)
# There is one observation that corresponds to 2014, let's remove it
air <- subset(air, as.integer(format(air$date, "%Y")) < 2014)

# Re-order the variables: date, pollutants and weather variables
air <- (air[, c(18, 1:6, 14:17, 7:13)])

# Re-arrange valid_obs. Keep the same variables as in 'air' and in the same
# order
valid_obs <- valid_obs[, c(18, 1:6, 14:17, 7:13)]
```


For the purpose of this project I will focus on five pollutants that should receive special attention according to the [World Health Organization](http://www.who.int/mediacentre/factsheets/fs313/en/)- WHO - and the accompanying weather conditions. I have decided to focus on the weather station that provided the largest amount of observations. All the pollutants indicated by the WHO have been registered since 2009, so I will focus on the five year period from 2009 to 2013. The final data set contains 43,823 observations on 17 variables. The pollutant variables that I will focus on are:

- so2 - Sulfur dioxide
- no2 - Nitrogen dioxide
- o3 - Ozone
- pm10 - Particulate matter smaller than 10 micrometers in diameter
- pm25 - Particulate matter smaller than 2.5 micrometers in diameter

****
 
## Exploratoy data analysis

Let's beging by having a look at the summary of the data.

```r
summary(air[, 2:ncol(air)])
```

```
##       so2              no             no2              co      
##  Min.   :  2.0   Min.   :  1.0   Min.   :  2.0   Min.   : 0.0  
##  1st Qu.:  2.0   1st Qu.:  4.0   1st Qu.: 19.0   1st Qu.: 0.3  
##  Median :  5.0   Median :  8.0   Median : 31.0   Median : 0.4  
##  Mean   :  6.2   Mean   : 20.1   Mean   : 35.4   Mean   : 0.5  
##  3rd Qu.:  8.0   3rd Qu.: 18.0   3rd Qu.: 47.0   3rd Qu.: 0.6  
##  Max.   :557.0   Max.   :916.0   Max.   :408.0   Max.   :21.6  
##  NA's   :589     NA's   :623     NA's   :609     NA's   :675   
##       pm10             o3             ben            tol       
##  Min.   :  0.0   Min.   :  0.0   Min.   : 0.0   Min.   :  0.0  
##  1st Qu.: 16.0   1st Qu.: 18.0   1st Qu.: 0.1   1st Qu.:  0.4  
##  Median : 23.0   Median : 39.0   Median : 0.2   Median :  1.1  
##  Mean   : 26.6   Mean   : 39.2   Mean   : 0.5   Mean   :  2.9  
##  3rd Qu.: 32.0   3rd Qu.: 57.0   3rd Qu.: 0.5   3rd Qu.:  2.5  
##  Max.   :888.0   Max.   :816.0   Max.   :22.5   Max.   :196.0  
##  NA's   :594     NA's   :624     NA's   :712    NA's   :719    
##       mxil            pm25             dd            vv      
##  Min.   :  0.0   Min.   :  1.0   Min.   :  0   Min.   : 0.1  
##  1st Qu.:  0.1   1st Qu.:  6.0   1st Qu.:  6   1st Qu.: 0.2  
##  Median :  0.2   Median : 10.0   Median :136   Median : 0.5  
##  Mean   :  1.2   Mean   : 12.3   Mean   :134   Mean   : 0.8  
##  3rd Qu.:  0.7   3rd Qu.: 16.0   3rd Qu.:225   3rd Qu.: 1.3  
##  Max.   :220.0   Max.   :592.0   Max.   :346   Max.   :17.1  
##  NA's   :741     NA's   :564     NA's   :413   NA's   :413   
##       tmp              hr             prb             rs      
##  Min.   :-10.0   Min.   : 22.0   Min.   : 900   Min.   :   5  
##  1st Qu.: 10.2   1st Qu.: 73.0   1st Qu.:1006   1st Qu.:   5  
##  Median : 14.1   Median : 84.0   Median :1012   Median :  54  
##  Mean   : 14.0   Mean   : 82.5   Mean   :1011   Mean   : 114  
##  3rd Qu.: 18.1   3rd Qu.: 95.0   3rd Qu.:1017   3rd Qu.: 125  
##  Max.   : 31.0   Max.   :100.0   Max.   :1034   Max.   :1126  
##  NA's   :425     NA's   :425     NA's   :425    NA's   :425   
##        ll      
##  Min.   : 0.0  
##  1st Qu.: 0.0  
##  Median : 0.0  
##  Mean   : 0.1  
##  3rd Qu.: 0.0  
##  Max.   :19.4  
##  NA's   :421
```

A quick look reveals that all variables have some missing observations, a minimum of 413 for 'dd' and 'vv', and a maxium of 741 for 'mxil'. This is not a problem though, they represent 1% to 1.7% of missing data per variable at most.

The second noticeable aspect is that the distribution of the pollutant variables present extreme outliers. This should be easier to see plotting histograms of their distributions.

### Pollutants

```r
p1 = ggplot(air, aes(x = so2)) + geom_histogram(binwidth = 1)

p2 = ggplot(air, aes(x = no2)) + geom_histogram(binwidth = 1)

p3 = ggplot(air, aes(x = o3)) + geom_histogram(binwidth = 1)

p4 = ggplot(air, aes(x = pm10)) + geom_histogram(binwidth = 1)

p5 = ggplot(air, aes(x = pm25)) + geom_histogram(binwidth = 1)

grid.arrange(p1, p2, p3, p4, p5, ncol = 5)
```

<img src="figure/Pollutants_histograms.png" title="plot of chunk Pollutants histograms" alt="plot of chunk Pollutants histograms" style="display: block; margin: auto;" />

The distribution of all of them display very distant extreme outliers which cause the distributions to be strongly right skewed. It is hard for me to know whether they represent accurate measurements of extreme cases that should be monitored, or whether they should be disregarded as anomalies.

Let's narrow it down using a 99 percentile.

```r
# The presence of NAs upsets the ploting New data frame without NAs
air_complete <- na.omit(air)

p1 = ggplot(air_complete, aes(x = so2)) + geom_histogram(binwidth = 1/2) + coord_cartesian(xlim = c(0, 
    quantile(air_complete$so2, 0.99)))

p2 = ggplot(air_complete, aes(x = no2)) + geom_histogram(binwidth = 1/2) + coord_cartesian(xlim = c(0, 
    quantile(air_complete$no2, 0.99)))

p3 = ggplot(air_complete, aes(x = o3)) + geom_histogram(binwidth = 1/2) + coord_cartesian(xlim = c(0, 
    quantile(air_complete$o3, 0.99)))

p4 = ggplot(air_complete, aes(x = pm10)) + geom_histogram(binwidth = 1/2) + 
    coord_cartesian(xlim = c(0, quantile(air_complete$pm10, 0.99)))

p5 = ggplot(air_complete, aes(x = pm25)) + geom_histogram(binwidth = 1/2) + 
    coord_cartesian(xlim = c(0, quantile(air_complete$pm25, 0.99)))

grid.arrange(p1, p2, p3, p4, p5, ncol = 5)
```

<img src="figure/Pollutants_histograms_0_95_quantile.png" title="plot of chunk Pollutants histograms 0.95 quantile" alt="plot of chunk Pollutants histograms 0.95 quantile" style="display: block; margin: auto;" />

By focusing on the 99 percentile we can have a better view on the distribution of each of the pollutants. They still appear right-skewed, only 'o3' presents a hump that makes its distribution flatter than the others. Despite the existence of extreme outliers, the distributions indicate that the median of pollution lies in the lower end of the scale, as confirmed numerically in the summary.

```r
summary(air[, c(2, 4, 7, 6, 11)])
```

```
##       so2             no2              o3             pm10      
##  Min.   :  2.0   Min.   :  2.0   Min.   :  0.0   Min.   :  0.0  
##  1st Qu.:  2.0   1st Qu.: 19.0   1st Qu.: 18.0   1st Qu.: 16.0  
##  Median :  5.0   Median : 31.0   Median : 39.0   Median : 23.0  
##  Mean   :  6.2   Mean   : 35.4   Mean   : 39.2   Mean   : 26.6  
##  3rd Qu.:  8.0   3rd Qu.: 47.0   3rd Qu.: 57.0   3rd Qu.: 32.0  
##  Max.   :557.0   Max.   :408.0   Max.   :816.0   Max.   :888.0  
##  NA's   :589     NA's   :609     NA's   :624     NA's   :594    
##       pm25      
##  Min.   :  1.0  
##  1st Qu.:  6.0  
##  Median : 10.0  
##  Mean   : 12.3  
##  3rd Qu.: 16.0  
##  Max.   :592.0  
##  NA's   :564
```

This is good as it means that pollution levels are usually on the lower end of the scale. But how does their distribution look over time?

### Time series of pollutants
To plot the time series of pollutants over time I decided to use the 'openair' package since it has some functionalities that come very handy to analyze this kind of data. Especially the possibility of easily getting different averaging times, which is very useful in order to check the levels of pollution against the limits recommended by the WHO. First, let's plot the daily average of pollutants over time.

```r
summaryPlot(air[,c(1, 2, 4, 7, 6, 11)],
            clip=TRUE, percentile= 0.99, # Use a 0.99 percentile to clip away extreme outliers
            na.len=24) # Display at least 24 hours of missing data
```

```
##     date1     date2       so2       no2        o3      pm10      pm25 
## "POSIXlt"  "POSIXt" "numeric" "numeric" "numeric" "integer" "integer"
```

<img src="figure/Pollutant_time_series.png" title="plot of chunk Pollutant time series" alt="plot of chunk Pollutant time series" style="display: block; margin: auto;" />

There is no clear pattern in any of the distributions except for some seasonality that is somehow expected. There are a few missing days of observations, which are marked in red below the time series. One in 2009, a few others in 2012 and another on in 2013. Interestingly, most of them occurred simultaneously for all variables. This might indicate a malfunction in the weather station.

#### SO2
In order to have a better look at the pattern, we should smooth the line plotted. Let's take a monthly average instead, starting with 'so2'.

```r
# Average data by months
air_month_avg <- timeAverage(air, avg.time = "month")

ggplot(air_month_avg, aes(x = date, y = so2)) + geom_line()
```

<img src="figure/SO2_time_series_monthly_average.png" title="plot of chunk SO2 time series monthly average" alt="plot of chunk SO2 time series monthly average" style="display: block; margin: auto;" />

The average level of 'so2' went down between 2010 and 2012, and somehow it seems to increase towards the end of each year, but it is difficult to spot a seasonal behaviour or any other pattern. The major sources of 'so2' are the combustion of fossil fuels at power plants and other industrial facilities, as indicated by the [United States Environmental Agency](http://www.epa.gov/air/sulfurdioxide/).

#### NO2

```r
ggplot(air_month_avg, aes(x = date, y = no2)) + geom_line()
```

<img src="figure/NO2_time_series_monthly_average.png" title="plot of chunk NO2 time series monthly average" alt="plot of chunk NO2 time series monthly average" style="display: block; margin: auto;" />

The main sources of 'no2' are internal combustion engines and thermal power stations, along with gas heaters and stoves, this seems to correlate with the strong seasonality observed in the plot. According to the [EPA](http://www.epa.gov/airquality/nitrogenoxides/), 'no2' also contributes to the formation of 'o3' and fine particule pollution, measured in this data by 'pm10' and 'pm25'. I will keep this relationship in mind when reviewing those variables.

Since the pollution levels of 'no2' are higher in the autumn and winter months, and lower in spring and summer, with marked minima in the summer, it would actually be interesting to plot them against the temperature values.

```r
ggplot(air_month_avg, aes(x = date, y = no2)) + geom_line() + geom_line(aes(y = air_month_avg$tmp), 
    color = "red")
```

<img src="figure/NO2_vs_TMP_time_series_monthly_average.png" title="plot of chunk NO2 vs TMP time series monthly average" alt="plot of chunk NO2 vs TMP time series monthly average" style="display: block; margin: auto;" />

The black line represents the levels of 'no2' and the red one the temperature, 'tmp'. They seem to correlate almost perfectly, as the temperature drops the levels of 'no2' rise. Let's see it a scatter plot.


```r
# Average data by days
air_day_avg <- timeAverage(air, avg.time = "day")
air_day_avg <- na.omit(air_day_avg)

# Average data by weeks
air_week_avg <- timeAverage(air, avg.time = "week")

# Average data by three months
air_season_avg <- timeAverage(air, avg.time = "quarter")

p1 = ggplot(air, aes(x = tmp, y = no2)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE) + ggtitle("Hourly values")

p2 = ggplot(air_day_avg, aes(x = tmp, y = no2)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE) + ggtitle("Daily average values")

p3 = ggplot(air_week_avg, aes(x = tmp, y = no2)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE) + ggtitle("Weekly average values")

p4 = ggplot(air_month_avg, aes(x = tmp, y = no2)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE) + ggtitle("Monthly average values")

p5 = ggplot(air_season_avg, aes(x = tmp, y = no2)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE) + ggtitle("Season average values")

grid.arrange(p1, p2, p3, p4, p5, ncol = 5)
```

```
## Warning: Removed 610 rows containing missing values (stat_smooth).
## Warning: Removed 610 rows containing missing values (geom_point).
```

<img src="figure/NO2_vs_TMP_correlation_scatterplots.png" title="plot of chunk NO2 vs TMP correlation scatterplots" alt="plot of chunk NO2 vs TMP correlation scatterplots" style="display: block; margin: auto;" />

```r

cor(air_complete$no2, air_complete$tmp)
```

```
## [1] -0.2231
```

```r
cor(air_day_avg$no2, air_day_avg$tmp)
```

```
## [1] -0.3276
```

```r
cor(air_week_avg$no2, air_week_avg$tmp)
```

```
## [1] -0.5313
```

```r
cor(air_month_avg$no2, air_month_avg$tmp)
```

```
## [1] -0.6845
```

```r
cor(air_season_avg$no2, air_season_avg$tmp)
```

```
## [1] -0.7394
```

It is quite surprising to see that their behaviour appears only correlated when averaging by longer time spans. Their correlation value increases from roughly -0.22 when looking at their hourly values, to -0.73 when plotting trimester averages. There is a certain amount of spread, but let's see hot a linear model would perform using the data averaged monthly.

```r
summary(lm(no2 ~ tmp, data = air_month_avg))
```

```
## 
## Call:
## lm(formula = no2 ~ tmp, data = air_month_avg)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -17.921  -4.552  -0.973   4.398  18.383 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)   56.465      3.074   18.37  < 2e-16 ***
## tmp           -1.510      0.211   -7.15  1.6e-09 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 6.85 on 58 degrees of freedom
## Multiple R-squared:  0.468,	Adjusted R-squared:  0.459 
## F-statistic: 51.1 on 1 and 58 DF,  p-value: 1.64e-09
```

It seems that 'tmp' is a significant predictor of 'no2' and the overall results of the linear model, an adjusted R-square of 0.4685, is not too bad for a single variable model. The model explains roughly 46% of the variation in the montly average values of 'no2'.

This relationship with 'tmp' suggests that I should go back and check the levels 'so2'.

#### SO2 Revisited
Let's plot 'so2' along with 'tmp'.

```r
ggplot(air_month_avg, aes(x = date, y = so2)) + geom_line() + geom_line(aes(y = air_month_avg$tmp), 
    color = "red")
```

<img src="figure/SO2_vs_TMP_time_series_monthly_average.png" title="plot of chunk SO2 vs TMP time series monthly average" alt="plot of chunk SO2 vs TMP time series monthly average" style="display: block; margin: auto;" />

Now the behaviour of 'so2' appears a bit clearer. Although the trend is more erratic than the one of 'no2', 'so2' seems also to oscillate according to 'tmp'. During the years 2010 to 2013 clearly the maxima 'tmp' values, in red, mark the 'so2' minima, and vice versa.


```r
p1 = ggplot(air_month_avg, aes(x = tmp, y = so2)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

p2 = ggplot(air_season_avg, aes(x = tmp, y = so2)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

grid.arrange(p1, p2, ncol = 2)
```

<img src="figure/SO2_vs_TMP_correlation_scatterplots.png" title="plot of chunk SO2 vs TMP correlation scatterplots" alt="plot of chunk SO2 vs TMP correlation scatterplots" style="display: block; margin: auto;" />

```r

cor(air_month_avg$so2, air_month_avg$tmp)
```

```
## [1] -0.435
```

```r
cor(air_season_avg$so2, air_season_avg$tmp)
```

```
## [1] -0.4695
```

There is a slight negative relationship, but there is a lot of scatter, which indicates that it would be perform very poorly in a linear model.

#### O3
How about the distribution of 'o3' over time?

```r
ggplot(air_month_avg, aes(x = date, y = o3)) + geom_line()
```

<img src="figure/O3_time_series_monthly_average.png" title="plot of chunk O3 time series monthly average" alt="plot of chunk O3 time series monthly average" style="display: block; margin: auto;" />

'o3' displays also a strong seasonal component so let's compare it agains temperature.

```r
ggplot(air_month_avg, aes(x = date, y = o3)) + geom_line() + geom_line(aes(y = air_month_avg$tmp), 
    color = "red")
```

<img src="figure/O3_vs_TMP_time_series_monthly_average.png" title="plot of chunk O3 vs TMP time series monthly average" alt="plot of chunk O3 vs TMP time series monthly average" style="display: block; margin: auto;" />

In this case, especially for the years 2010 to 2013, the behaviour of 'o3' and 'tmp' show an interesting time delay. The highest levels of 'o3' per year can be observed right before the highest values of 'tmp', as if the rise in 'o3' preceded the behaviour of 'tmp' throughout the year.

```r
ggplot(air_month_avg, aes(x = date, y = o3)) + geom_line(size = 1) + geom_line(aes(y = air_month_avg$tmp * 
    3), color = "red", linetype = 2)
```

<img src="figure/O3_vs_TMP_x_3_time_series_monthly_average.png" title="plot of chunk O3 vs TMP x 3 time series monthly average" alt="plot of chunk O3 vs TMP x 3 time series monthly average" style="display: block; margin: auto;" />

It is interesting that multiplying the temperature value by three and overplotting it together with the values of 'o3'. Both series evolve together with 'tmp' showing a slight time delay.
Accroding to the [EPA](http://www.epa.gov/airquality/ozonepollution/basic.html) ground level ozone is likely to reach unhealthy levels on hot sunny days in urban environments. The plot shows however that the peaks in ozone are reached slightly earlier that the peaks in temperature. Could it be that solar radiation is a better explanatory variable?


```r
p1 = ggplot(air_month_avg, aes(x = date, y = o3)) + geom_line(size = 1) + geom_line(aes(y = air_month_avg$rs), 
    color = "red", linetype = 2) + ggtitle("o3 and rs")

p2 = ggplot(air_month_avg, aes(x = date, y = o3)) + geom_line(size = 1) + geom_line(aes(y = air_month_avg$rs/3), 
    color = "red", linetype = 2) + ggtitle("o3 and rs / 3")

grid.arrange(p1, p2, ncol = 1)
```

<img src="figure/O3_vs_RS_time_series_monthly_average.png" title="plot of chunk O3 vs RS time series monthly average" alt="plot of chunk O3 vs RS time series monthly average" style="display: block; margin: auto;" />

The levels of solar radiation in the first plot are far higher than those of o3. So I divided the 'rs' levels by three and overplotted them with 'o3'. 'rs' matches pretty well the 'o3' levels and the time delay dissapears.
we can compare them side by side in a scatterplot.

```r
p1 = ggplot(air_month_avg, aes(x = o3, y = tmp)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

p2 = ggplot(air_month_avg, aes(x = o3, y = rs)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

grid.arrange(p1, p2, ncol = 2)
```

<img src="figure/O3_vs_TMP_scatterplot.png" title="plot of chunk O3 vs TMP scatterplot" alt="plot of chunk O3 vs TMP scatterplot" style="display: block; margin: auto;" />

'rs' is definitely a better indicator to follow 'o3' levels. There is a lot of scatter but it is much better than that of 'tmp'.

How about the claim made by the EPA that ground level ozone is the result of reactions between oxides of nitrogen, such as 'no' and 'no2', and volatile organic compounds, such as 'ben'?

```r
p1 = ggplot(air_month_avg, aes(x = no, y = o3)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

p2 = ggplot(air_month_avg, aes(x = no2, y = o3)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

p3 = ggplot(air_month_avg, aes(x = ben, y = o3)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

grid.arrange(p1, p2, p3, ncol = 3)
```

<img src="figure/O3_~_NO___NO2___BEN_scatterplots.png" title="plot of chunk O3 ~ NO + NO2 + BEN scatterplots" alt="plot of chunk O3 ~ NO + NO2 + BEN scatterplots" style="display: block; margin: auto;" />

There is a negative relationship between all of them but there is also a lot of scatter, indicating that their linear relationship is weak, especially that of 'ben'. Again, let's build a linear model to test their relationship using the monthly averages.


```r
summary(lm(o3 ~ no + no2 + ben, data = air_month_avg))
```

```
## 
## Call:
## lm(formula = o3 ~ no + no2 + ben, data = air_month_avg)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -18.430  -4.633  -0.189   4.571  13.653 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)   56.954      4.960   11.48  2.4e-16 ***
## no            -0.488      0.160   -3.05  0.00346 ** 
## no2           -0.518      0.193   -2.69  0.00933 ** 
## ben           22.544      5.860    3.85  0.00031 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 7.05 on 56 degrees of freedom
## Multiple R-squared:  0.599,	Adjusted R-squared:  0.577 
## F-statistic: 27.9 on 3 and 56 DF,  p-value: 3.7e-11
```

The adjusted R-square value is surprisingly high, which indicates that it is quite a good model. By combining these three variables the resulting model explains almost 60% of the variation in 'o3'.

Since higher levels of 'o3' are expected during hot sunny days and we have already inspected the relationship with the 'rs', let's add it to the model.

```r
summary(lm(o3 ~ no + no2 + ben + rs, data = air_month_avg))
```

```
## 
## Call:
## lm(formula = o3 ~ no + no2 + ben + rs, data = air_month_avg)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -18.191  -4.312  -0.397   4.349  14.364 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)  52.3274     7.8660    6.65  1.4e-08 ***
## no           -0.4515     0.1672   -2.70   0.0092 ** 
## no2          -0.4620     0.2070   -2.23   0.0297 *  
## ben          21.0378     6.2077    3.39   0.0013 ** 
## rs            0.0228     0.0300    0.76   0.4506    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 7.08 on 55 degrees of freedom
## Multiple R-squared:  0.603,	Adjusted R-squared:  0.574 
## F-statistic: 20.9 on 4 and 55 DF,  p-value: 1.63e-10
```

The model actually performs slightly worse. How about 'tmp'?


```r
summary(lm(o3 ~ no + no2 + ben + tmp, data = air_month_avg))
```

```
## 
## Call:
## lm(formula = o3 ~ no + no2 + ben + tmp, data = air_month_avg)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -17.810  -3.730   0.612   4.870  11.767 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)   74.842      8.198    9.13  1.3e-12 ***
## no            -0.538      0.153   -3.52  0.00087 ***
## no2           -0.703      0.195   -3.60  0.00069 ***
## ben           23.119      5.569    4.15  0.00012 ***
## tmp           -0.761      0.285   -2.67  0.01006 *  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 6.7 on 55 degrees of freedom
## Multiple R-squared:  0.645,	Adjusted R-squared:  0.619 
## F-statistic:   25 on 4 and 55 DF,  p-value: 8.17e-12
```

Temperature seems a better explanatory variable despite the time delay, although its probability value shows that it is not as significant as the others. Let's keep it for the time being. In addition to it we can assume that sunny days are hotter depending on additional weather conditions, such as wind speed and relative humidity, so let's add then one by one. 'vv' first.

```r
summary(lm(o3 ~ no + no2 + ben + tmp + vv, data = air_month_avg))
```

```
## 
## Call:
## lm(formula = o3 ~ no + no2 + ben + tmp + vv, data = air_month_avg)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -14.260  -4.008   0.082   3.700  13.743 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)   51.447     11.029    4.66  2.1e-05 ***
## no            -0.484      0.144   -3.35  0.00147 ** 
## no2           -0.470      0.199   -2.36  0.02214 *  
## ben           19.582      5.350    3.66  0.00057 ***
## tmp           -0.561      0.276   -2.04  0.04665 *  
## vv            15.891      5.379    2.95  0.00464 ** 
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 6.27 on 54 degrees of freedom
## Multiple R-squared:  0.694,	Adjusted R-squared:  0.666 
## F-statistic: 24.5 on 5 and 54 DF,  p-value: 8.57e-13
```

The model has increased its predicting power although the significance of 'no2' has gone down. Interestingly, although one would think that wind would dissipate and clear the air, 'vv' is positively correlated with 'o3', which means that higher wind speeds result in higher levels of 'o3'. This is also mentioned by the EPA, who states that 'o3' can be carried over long distances by the wind. Let's see it on a scatterplot.


```r
# The openair package requires sometimes the variables as established in the
# package so let's create a new data frame and enter the variables as
# expected
p1 = ggplot(air_day_avg, aes(x = vv, y = o3)) + geom_point() + ggtitle("Day average values")

p2 = ggplot(air_month_avg, aes(x = vv, y = o3)) + geom_point() + ggtitle("Month average values")

grid.arrange(p1, p2, ncol = 2)
```

<img src="figure/O3_and_wind_speed.png" title="plot of chunk O3 and wind speed" alt="plot of chunk O3 and wind speed" style="display: block; margin: auto;" />



Now let's replace 'vv' by 'hr'.

```r
summary(lm(o3 ~ no + no2 + ben + tmp + hr, data = air_month_avg))
```

```
## 
## Call:
## lm(formula = o3 ~ no + no2 + ben + tmp + hr, data = air_month_avg)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -18.298  -3.892   0.779   5.115  12.976 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)  68.9188    14.2893    4.82  1.2e-05 ***
## no           -0.5375     0.1539   -3.49  0.00096 ***
## no2          -0.6861     0.1996   -3.44  0.00114 ** 
## ben          22.9539     5.6163    4.09  0.00015 ***
## tmp          -0.7508     0.2879   -2.61  0.01177 *  
## hr            0.0637     0.1255    0.51  0.61368    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 6.74 on 54 degrees of freedom
## Multiple R-squared:  0.646,	Adjusted R-squared:  0.614 
## F-statistic: 19.7 on 5 and 54 DF,  p-value: 3.89e-11
```

Adding 'hr' results in a weaker model and it is clear that 'hr' is not a significant predictor of 'o3', so let's stick to the previous one.

```r
o3_model <- lm(o3 ~ no + no2 + ben + tmp + vv, data = air_month_avg)
summary(o3_model)
```

```
## 
## Call:
## lm(formula = o3 ~ no + no2 + ben + tmp + vv, data = air_month_avg)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -14.260  -4.008   0.082   3.700  13.743 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)   51.447     11.029    4.66  2.1e-05 ***
## no            -0.484      0.144   -3.35  0.00147 ** 
## no2           -0.470      0.199   -2.36  0.02214 *  
## ben           19.582      5.350    3.66  0.00057 ***
## tmp           -0.561      0.276   -2.04  0.04665 *  
## vv            15.891      5.379    2.95  0.00464 ** 
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 6.27 on 54 degrees of freedom
## Multiple R-squared:  0.694,	Adjusted R-squared:  0.666 
## F-statistic: 24.5 on 5 and 54 DF,  p-value: 8.57e-13
```

The model explains 69.42 % of the variability in ozone levels using the monthly average data. It is not a very robust model but it has increased its predicting power over very few steps using the data available.

### PM10 and PM25
'pm10' and 'pm25' refer to the measurement of the same chemicals, which differ only in size; smaller than 10 micrograms, and smaller than 2.5 micrograms. It makes sense to plot them together as it is expected that they share a lot of characteristics.

```r
ggplot(air_month_avg, aes(x = date, y = pm10)) + geom_line() + geom_line(aes(y = air_month_avg$pm25), 
    color = "blue")
```

<img src="figure/PM10_time_series_monthly_average.png" title="plot of chunk PM10 time series monthly average" alt="plot of chunk PM10 time series monthly average" style="display: block; margin: auto;" />

'pm10' in black and 'pm25' in blue, it is evident that both variables follow the same pattern, as it was expected. There seem to be some seasonal fluctuations, although it becomes quite erratic by 2012. In any case,let's plot it against 'tmp'.


```r
ggplot(air_month_avg, aes(x = date, y = pm10)) + geom_line() + geom_line(aes(y = air_month_avg$pm25), 
    color = "blue") + geom_line(aes(y = air_month_avg$tmp), color = "red", alpha = 0.75)
```

<img src="figure/PM10_vs_TMP_time_series_monthly_average.png" title="plot of chunk PM10 vs TMP time series monthly average" alt="plot of chunk PM10 vs TMP time series monthly average" style="display: block; margin: auto;" />

There seems to be some seasonality involved. The minima of 'pm10' and 'pm25' during the period 2009 to 2011 matches the 'tmp' maxima. However, this relationship is very irregular and does not continue into 2012 and 2013. Let's plot thei relationship in scatterplots.


```r
p1 = ggplot(air_season_avg, aes(x = tmp, y = pm10)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

p2 = ggplot(air_season_avg, aes(x = tmp, y = pm25)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

grid.arrange(p1, p2, ncol = 2)
```

<img src="figure/PM10_vs_TMP_scatterplot.png" title="plot of chunk PM10 vs TMP scatterplot" alt="plot of chunk PM10 vs TMP scatterplot" style="display: block; margin: auto;" />

The scatter plot also shows that there is indeed a very weak relationship between the two.

Particulate matter, such as 'pm10' and 'pm25', is made up of different components, according to the [EPA](http://www.epa.gov/airquality/particlepollution/), especially nitrates, sulfates, organic chemicals, metals and soil or dust particles. The variables 'ben', 'tol' and 'mxil' are organic compounds. The EPA also indicates that 'no2' is related to particulate matter levels. So let's check their relationships.

```r
p1 = ggplot(air_season_avg, aes(x = ben, y = pm10)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

p2 = ggplot(air_season_avg, aes(x = tol, y = pm10)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

p3 = ggplot(air_season_avg, aes(x = mxil, y = pm10)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

p4 = ggplot(air_season_avg, aes(x = no2, y = pm10)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

p5 = ggplot(air_season_avg, aes(x = ben, y = pm25)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

p6 = ggplot(air_season_avg, aes(x = tol, y = pm25)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

p7 = ggplot(air_season_avg, aes(x = mxil, y = pm25)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

p8 = ggplot(air_season_avg, aes(x = no2, y = pm25)) + geom_jitter() + geom_smooth(method = lm, 
    se = TRUE)

grid.arrange(p1, p2, p3, p4, p5, p6, p7, p8, ncol = 4)
```

<img src="figure/PM10_and_PM25_vs_BEN_TOL_and_MXIL_scatterplots.png" title="plot of chunk PM10 and PM25 vs BEN TOL and MXIL scatterplots" alt="plot of chunk PM10 and PM25 vs BEN TOL and MXIL scatterplots" style="display: block; margin: auto;" />

As it is clear in the plots there is not a strong linear relationship between the variables. Especially in the case of 'tol' and 'mxil'. In any case, let's see how they would perform on a linear model.

```r
summary(lm(pm10 ~ ben + tol + mxil + no2, data = air_month_avg))
```

```
## 
## Call:
## lm(formula = pm10 ~ ben + tol + mxil + no2, data = air_month_avg)
## 
## Residuals:
##    Min     1Q Median     3Q    Max 
## -6.537 -2.069  0.159  1.775  9.947 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)  18.1543     1.9228    9.44  4.2e-13 ***
## ben          13.0984     3.1443    4.17  0.00011 ***
## tol          -0.1790     0.1997   -0.90  0.37387    
## mxil         -0.3028     0.3199   -0.95  0.34810    
## no2           0.0941     0.0614    1.53  0.13095    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 3.5 on 55 degrees of freedom
## Multiple R-squared:  0.406,	Adjusted R-squared:  0.363 
## F-statistic: 9.39 on 4 and 55 DF,  p-value: 7.37e-06
```

The model shows that there is a weak positive relationship between the variables. It confirms also the weaker relationship with 'tol', 'mxil' and 'no2'.

### World Health Organisation guidelines
The [WHO](http://www.who.int/mediacentre/factsheets/fs313/en/) provides also guidelines about the recommended levels of pollution and various thresholds to monitor pollutants. Let's use them to check what is the air quality in the city.

#### SO2
WHO guideline values:
- Annual mean of 20 micrograms / cubic meter
- 1-hour mean of 500 micrograms / cubic meter. **Unfortunately there is no data available to compute 1-hour mean**

```r
# Annual mean
air_year_avg <- timeAverage(air, avg.time = "year")

ggplot(air_year_avg, aes(x = date, y = so2)) + geom_point(size = 4) + geom_line(aes(y = so2), 
    linetype = 3) + geom_hline(yintercept = 20, color = "red") + coord_cartesian(ylim = c(0, 
    21))
```

<img src="figure/WHO_SO2_Levels.png" title="plot of chunk WHO SO2 Levels" alt="plot of chunk WHO SO2 Levels" style="display: block; margin: auto;" />

During the period 2009 to 2013, the annual mean of 'so2' has been way below the WHO recommendations.

#### NO2
WHO guideline values:
- Annual mean of 40 micrograms / cubic meter
- 1-hour mean of 200 micrograms / cubic meter. **Unfortunately there is no data available to compute 1-hour mean**

```r
ggplot(air_year_avg, aes(x = date, y = no2)) + geom_point(size = 4) + geom_line(aes(y = no2), 
    linetype = 3) + geom_hline(yintercept = 40, color = "red") + coord_cartesian(ylim = c(0, 
    41))
```

<img src="figure/WHO_NO2_Levels.png" title="plot of chunk WHO NO2 Levels" alt="plot of chunk WHO NO2 Levels" style="display: block; margin: auto;" />

The annual mean of 'no2' has also been below the WHO reccomendations, although not too far.

#### O3
WHO guideline values:
- 8-hour mean of 100 micrograms / cubic meter

```r
# 8-hour mean
air_8h_avg <- timeAverage(air, avg.time = "8 hour")

ggplot(air_8h_avg, aes(x = date, y = o3)) + geom_line() + geom_hline(yintercept = 100, 
    color = "red") + coord_cartesian(ylim = c(0, 120))
```

<img src="figure/WHO_O3_Levels.png" title="plot of chunk WHO O3 Levels" alt="plot of chunk WHO O3 Levels" style="display: block; margin: auto;" />

The seasonal behaviour of 'o3' can be clearly seen once more in this 8-hour mean plot and on few ocassions the levels of 'o3' have surpassed the limits established by the WHO as dangerous.

#### PM10
WHO guideline values:
- Annual mean of 20 micrograms / cubic meter
- 24-hour mean of 50 micrograms / cubic meter

```r
ggplot(air_year_avg, aes(x = date, y = pm10)) + geom_point(size = 4) + geom_line(aes(y = pm10), 
    linetype = 3) + geom_hline(yintercept = 20, color = "red") + coord_cartesian(ylim = c(0, 
    30)) + ggtitle("Annual mean")
```

<img src="figure/WHO_PM10_annual_levels.png" title="plot of chunk WHO PM10 annual levels" alt="plot of chunk WHO PM10 annual levels" style="display: block; margin: auto;" />




```r
ggplot(air_day_avg, aes(x = date, y = pm10)) + geom_line() + geom_hline(yintercept = 50, 
    color = "red") + coord_cartesian(ylim = c(0, 170)) + ggtitle("24-hour mean")
```

<img src="figure/WHO_PM10_24-h_levels.png" title="plot of chunk WHO PM10 24-h levels" alt="plot of chunk WHO PM10 24-h levels" style="display: block; margin: auto;" />

The 24-hour plot shows how the 'pm10' levels surpass quite often the WHO limit and the annual mean plot 
shows how consistently, year after year, the 'pm10' levels are above the limits. According to the WHO this may lead to significant health risks.

#### PM25
WHO guideline values:
- Annual mean of 10 micrograms / cubic meter
- 24-hour mean of 25 micrograms / cubic meter

```r
ggplot(air_year_avg, aes(x = date, y = pm25)) + geom_point(size = 4) + geom_line(aes(y = pm25), 
    linetype = 3) + geom_hline(yintercept = 10, color = "red") + coord_cartesian(ylim = c(0, 
    15)) + ggtitle("Annual mean")
```

<img src="figure/WHO_PM25_annual_levels.png" title="plot of chunk WHO PM25 annual levels" alt="plot of chunk WHO PM25 annual levels" style="display: block; margin: auto;" />



```r
ggplot(air_day_avg, aes(x = date, y = pm25)) + geom_line() + geom_hline(yintercept = 25, 
    color = "red") + coord_cartesian(ylim = c(0, 110)) + ggtitle("24-hour mean")
```

<img src="figure/WHO_PM25_24-h_levels.png" title="plot of chunk WHO PM25 24-h levels" alt="plot of chunk WHO PM25 24-h levels" style="display: block; margin: auto;" />

As in the previous case, the levels of 'pm25' surpass also the WHO limits several times on a 24-hour mean basis, and the annual mean is consistently above the limit for the whole period.

### Pollutants distribution over the week
An interesting way of looking at pollution is over their weekly levels. Given the existence of extreme outliers I will restrict the values to include up to the 99 percentile.

```r
# Create weekday variable
air$weekday <- weekdays(air$date)
# Order the weekday variable
air$weekday = factor(air$weekday, levels = c("Monday", "Tuesday", "Wednesday", 
    "Thursday", "Friday", "Saturday", "Sunday"), ordered = TRUE)

# Melt to get the pollutants by weekday
air_melt_week_pollutants <- melt(air[, c(19, 2, 4, 7, 6, 11)])
```

```
## Using weekday as id variables
```

```r
air_melt_week_pollutants <- na.omit(air_melt_week_pollutants)  # Remove NA values

# Create new data frame to include only the pollutant needed in order to
# successfully use the quantile function SO2 subset
air_melt_week_so2 <- subset(air_melt_week_pollutants, variable == "so2")

p1 = ggplot(air_melt_week_so2, aes(x = weekday, y = value)) + geom_boxplot() + 
    coord_cartesian(ylim = c(0, quantile(air_melt_week_so2$value, 0.99))) + 
    ylab("so2")

# NO2 subset
air_melt_week_no2 <- subset(air_melt_week_pollutants, variable == "no2")

p2 = ggplot(air_melt_week_no2, aes(x = weekday, y = value)) + geom_boxplot() + 
    coord_cartesian(ylim = c(0, quantile(air_melt_week_no2$value, 0.99))) + 
    ylab("no2")

# O3 subset
air_melt_week_o3 <- subset(air_melt_week_pollutants, variable == "o3")

p3 = ggplot(air_melt_week_o3, aes(x = weekday, y = value)) + geom_boxplot() + 
    coord_cartesian(ylim = c(0, quantile(air_melt_week_o3$value, 0.99))) + ylab("o3")

# PM10 subset
air_melt_week_pm10 <- subset(air_melt_week_pollutants, variable == "pm10")

p4 = ggplot(air_melt_week_pm10, aes(x = weekday, y = value)) + geom_boxplot() + 
    coord_cartesian(ylim = c(0, quantile(air_melt_week_pm10$value, 0.99))) + 
    ylab("pm10")

# PM25 subset
air_melt_week_pm25 <- subset(air_melt_week_pollutants, variable == "pm25")

p5 = ggplot(air_melt_week_pm25, aes(x = weekday, y = value)) + geom_boxplot() + 
    coord_cartesian(ylim = c(0, quantile(air_melt_week_pm25$value, 0.99))) + 
    ylab("pm25")

grid.arrange(p1, p2, p3, p4, p5, ncol = 3)
```

<img src="figure/Pollutant_boxplot_by_w.png" title="plot of chunk Pollutant boxplot by w" alt="plot of chunk Pollutant boxplot by w" style="display: block; margin: auto;" />

Even using the 99 percentile some distributions show a large number of outliers. This is especially the case of 'so2', whose distribution does not vary much over the week. 'no2', however, decreases significantly during the weekends. 'pm10' and 'pm25' although are related to 'no2' levels shrink slightly their IQR, but their means remain quite constant over the week.

'o3' is the only pollutant that seems to increase over the weekend. As we saw before, 'no2' might explain the behaviour of 'o3'. Therefore, the decrease of 'no2' over the weekend could be associated with the increase in 'o3'. Other factors influencing it were 'no', 'ben', 'tmp' and 'vv'. Here is the model again:

```r
summary(o3_model)
```

```
## 
## Call:
## lm(formula = o3 ~ no + no2 + ben + tmp + vv, data = air_month_avg)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -14.260  -4.008   0.082   3.700  13.743 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)   51.447     11.029    4.66  2.1e-05 ***
## no            -0.484      0.144   -3.35  0.00147 ** 
## no2           -0.470      0.199   -2.36  0.02214 *  
## ben           19.582      5.350    3.66  0.00057 ***
## tmp           -0.561      0.276   -2.04  0.04665 *  
## vv            15.891      5.379    2.95  0.00464 ** 
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 6.27 on 54 degrees of freedom
## Multiple R-squared:  0.694,	Adjusted R-squared:  0.666 
## F-statistic: 24.5 on 5 and 54 DF,  p-value: 8.57e-13
```


To check once more the results shown. I will focus on the evolution of 'no2', 'no' and 'ben' over the week. I will leave aside the weather conditions since they are not related to week patterns.

```r
# Melt to get 'no' and 'ben' by weekday
air_melt_week_no_ben <- melt(air[, c(19, 4, 3, 8)])
```

```
## Using weekday as id variables
```

```r
air_melt_week_no_ben <- na.omit(air_melt_week_no_ben)  # Remove NA values


p1 = ggplot(air_melt_week_no2, aes(x = weekday, y = value)) + geom_boxplot() + 
    coord_cartesian(ylim = c(0, quantile(air_melt_week_no2$value, 0.99))) + 
    ylab("no2")

# NO subset
air_melt_week_no <- subset(air_melt_week_no_ben, variable == "no")

p2 = ggplot(air_melt_week_no, aes(x = weekday, y = value)) + geom_boxplot() + 
    coord_cartesian(ylim = c(0, quantile(air_melt_week_no$value, 0.99))) + ylab("no")

# BEN subset
air_melt_week_ben <- subset(air_melt_week_no_ben, variable == "ben")

p3 = ggplot(air_melt_week_ben, aes(x = weekday, y = value)) + geom_boxplot() + 
    coord_cartesian(ylim = c(0, quantile(air_melt_week_ben$value, 0.99))) + 
    ylab("ben")

grid.arrange(p1, p2, p3, ncol = 3)
```

<img src="figure/NO_and_BEN_boxplot_by_w.png" title="plot of chunk NO and BEN boxplot by w" alt="plot of chunk NO and BEN boxplot by w" style="display: block; margin: auto;" />

The levels of 'no2' display the stronger decrease. The IQR of 'no' and 'ben' shrink, but their medians remain quite constant. It is also noticeable that even using a 99 percentile distribution they are both strongly right skewed distributions.

### 24-hour time and pollution
How about the distribution of pollutants over 24-hours? Again I will take into consideration a 99 percentile.

```r
# Create a time variable with the hours
air$time <- as.factor(format(air$date, "%H"))

# Melt to get the pollutants by hour
air_melt_24h_pollutants <- melt(air[, c(20, 2, 4, 7, 6, 11)])
```

```
## Using time as id variables
```

```r
air_melt_24h_pollutants <- na.omit(air_melt_24h_pollutants)  # Clear NA values

# SO2 subset
air_melt_24h_so2 <- subset(air_melt_24h_pollutants, variable == "so2")

p1 = ggplot(air_melt_24h_so2, aes(x = time, y = value)) + geom_boxplot() + coord_cartesian(ylim = c(0, 
    quantile(air_melt_24h_so2$value, 0.99))) + ylab("so2")

# NO2 subset
air_melt_24h_no2 <- subset(air_melt_24h_pollutants, variable == "no2")

p2 = ggplot(air_melt_24h_no2, aes(x = time, y = value)) + geom_boxplot() + coord_cartesian(ylim = c(0, 
    quantile(air_melt_24h_no2$value, 0.99))) + ylab("no2")

# O3 subset
air_melt_24h_o3 <- subset(air_melt_24h_pollutants, variable == "o3")

p3 = ggplot(air_melt_24h_o3, aes(x = time, y = value)) + geom_boxplot() + coord_cartesian(ylim = c(0, 
    quantile(air_melt_24h_o3$value, 0.99))) + ylab("o3")

# PM10 subset
air_melt_24h_pm10 <- subset(air_melt_24h_pollutants, variable == "pm10")

p4 = ggplot(air_melt_24h_pm10, aes(x = time, y = value)) + geom_boxplot() + 
    coord_cartesian(ylim = c(0, quantile(air_melt_24h_pm10$value, 0.99))) + 
    ylab("pm10")

# PM25 subset
air_melt_24h_pm25 <- subset(air_melt_24h_pollutants, variable == "pm25")

p5 = ggplot(air_melt_24h_pm25, aes(x = time, y = value)) + geom_boxplot() + 
    coord_cartesian(ylim = c(0, quantile(air_melt_24h_pm25$value, 0.99))) + 
    ylab("pm25")

grid.arrange(p1, p2, p3, p4, p5, ncol = 3)
```

<img src="figure/Hour_and_pollution_boxplot.png" title="plot of chunk Hour and pollution boxplot" alt="plot of chunk Hour and pollution boxplot" style="display: block; margin: auto;" />

Let's review them one by one:
- The levels of 'so2' do not vary greatly over a 24-hour period. The IQR increases during the morning but the median remains quite stable, until it decreases slighlty towards the evening and night.
- 'no2' displays a wide variation. It reaches its maximum values around 08:00 and again at 19:00. This behaviour seems to coincide with times of heavier traffic, which are related in turn with the working hours in the city. **Note** that in Spain most companies work from 09:00 to 19:00 with a recess of a couple of hours for lunchtime. Heavier traffic is expected right between 08:00 to 09:00 and later on between 19:00 to 20:00.
- 'o3' also displays a very variable behaviour. It decreases slowly during the night until it reaches its minimum at 07:00 to rise again to its maximum at 15:00.
- While 'pm25' remains quite constant, 'pm10' seems to decrease slightly over the early hours of the day, between 00:00 and 04:00, and then it rises again.


### Week and time visualization
Pollution is the result of human activity, and human activity follows distinctive patterns over the week so let's analyse the variability of polutants by weekday and time.

### SO2

```r
# Melt to get weekday, time and pollutants
air_pollutants_week_24h <- melt(air[, c(19, 20, 2, 4, 7, 6, 11)])
```

```
## Using weekday, time as id variables
```

```r
air_pollutants_week_24h <- na.omit(air_pollutants_week_24h)  # Remove NAs

# SO2 subset
air_so2_week_24h <- subset(air_pollutants_week_24h, variable == "so2")

ggplot(air_so2_week_24h, aes(x = time, y = value)) + geom_boxplot() + facet_wrap(~weekday, 
    ncol = 7) + coord_cartesian(ylim = c(0, quantile(air_so2_week_24h$value, 
    0.99))) + ggtitle("so2")
```

<img src="figure/SO2_by_weekday_and_hour.png" title="plot of chunk SO2 by weekday and hour" alt="plot of chunk SO2 by weekday and hour" style="display: block; margin: auto;" />

This visualization gets a bit cluttered but there seems to be a pattern. Leaving aside the outliers and focusing on the median and IQR, on working days the levels of 'so2' increase during the day and decrease during the night. On weekends the they don't rise as high during the day and decrease much more during the night.

### NO2

```r
# NO2 subset
air_no2_week_24h <- subset(air_pollutants_week_24h, variable == "no2")

ggplot(air_no2_week_24h, aes(x = time, y = value)) + geom_boxplot() + facet_wrap(~weekday, 
    ncol = 7) + coord_cartesian(ylim = c(0, quantile(air_no2_week_24h$value, 
    0.99))) + ggtitle("no2")
```

<img src="figure/NO2_by_weekday_and_hour.png" title="plot of chunk NO2 by weekday and hour" alt="plot of chunk NO2 by weekday and hour" style="display: block; margin: auto;" />

As we have seen before, 'no2' seems strongly related with the rush hours of heavy traffic, morning and evening. This pattern is displayed during the working weekdays but not during the weekends. During the weekends the maximum values are reached during the evening alone, but they don't rise as high as in working days.

### O3

```r
# O3 subset
air_o3_week_24h <- subset(air_pollutants_week_24h, variable == "o3")

ggplot(air_o3_week_24h, aes(x = time, y = value)) + geom_boxplot() + facet_wrap(~weekday, 
    ncol = 7) + coord_cartesian(ylim = c(0, quantile(air_o3_week_24h$value, 
    0.99))) + ggtitle("o3")
```

<img src="figure/O3_by_weekday_and_hour.png" title="plot of chunk O3 by weekday and hour" alt="plot of chunk O3 by weekday and hour" style="display: block; margin: auto;" />

'o3' seems to perform the same oscillation throughout the entire week. However, it is noticeable how on Saturday the early morning dip is less pronounced, and it disappears altogether on Sundays. It also interesting that highest values are reached during the weekend.

### PM10

```r
# PM10 subset
air_pm10_week_24h <- subset(air_pollutants_week_24h, variable == "pm10")

ggplot(air_pm10_week_24h, aes(x = time, y = value)) + geom_boxplot() + facet_wrap(~weekday, 
    ncol = 7) + coord_cartesian(ylim = c(0, quantile(air_pm10_week_24h$value, 
    0.99))) + ggtitle("pm10")
```

<img src="figure/PM10_by_weekday_and_hour.png" title="plot of chunk PM10 by weekday and hour" alt="plot of chunk PM10 by weekday and hour" style="display: block; margin: auto;" />

'pm10' also shows a week-related pattern. Over the working weekdays it reaches its maximum value between 08:00 and 10:00, but during the weekend its distribution only rises slightly during the night, just as it does during the entire week.

### PM25

```r
# PM25 subset
air_pm25_week_24h <- subset(air_pollutants_week_24h, variable == "pm25")

ggplot(air_pm25_week_24h, aes(x = time, y = value)) + geom_boxplot() + facet_wrap(~weekday, 
    ncol = 7) + coord_cartesian(ylim = c(0, quantile(air_pm25_week_24h$value, 
    0.99))) + ggtitle("pm25")
```

<img src="figure/PM25_by_weekday_and_hour.png" title="plot of chunk PM25 by weekday and hour" alt="plot of chunk PM25 by weekday and hour" style="display: block; margin: auto;" />

'pm25' behaves pretty much as 'pm10'. On working weekdays it raises until 08:00 to 10:00, but that peak dissapears during the weekend and its levels rise slightly only during the night.

### Pollution Heatmaps
Another interesting visualization tool that is made very easy in the 'openair' package are heatmaps. The heatmaps display years in a grid whose x axis are months and y axis represents the time of the day.

#### SO2

```r
trendLevel(air, "so2", layout = c(5, 1))
```

<img src="figure/SO2_heatmap.png" title="plot of chunk SO2 heatmap" alt="plot of chunk SO2 heatmap" style="display: block; margin: auto;" />

The mean of 'so2' by hour was highest during 2009, while 2011 displays the lowest mean levels, however it rised up again in 2012, and significantly on December 2013.

#### NO2

```r
trendLevel(air, "no2", layout = c(5, 1))
```

<img src="figure/NO2_heatmap.png" title="plot of chunk NO2 heatmap" alt="plot of chunk NO2 heatmap" style="display: block; margin: auto;" />

The mean of 'no2' displays a pattern easy to follow. It decreases from April until September and reaches its maxima from November until February. As we saw before the highest concentrations occur around 08:00 - 09:00 and later around 19:00 - 20:00.

#### O3

```r
trendLevel(air, "o3", layout = c(5, 1))
```

<img src="figure/O3_heatmap.png" title="plot of chunk O3 heatmap" alt="plot of chunk O3 heatmap" style="display: block; margin: auto;" />

'o3' also displays a very strong pattern. It peaks around the afternoon of spring and summer months. It decreases to its lowest values during the early morning, 03:00 till 08:00.

#### PM10

```r
trendLevel(air, "pm10", layout = c(5, 1))
```

<img src="figure/PM__heatmap.png" title="plot of chunk PM!=heatmap" alt="plot of chunk PM!=heatmap" style="display: block; margin: auto;" />

There is not a clear trend in the mean values of 'pm10', but its concentrations seem to diminish over time, and by 2013 there are very few high concentrations.

#### PM25

```r
trendLevel(air, "pm25", layout = c(5, 1))
```

<img src="figure/PM25_heatmap.png" title="plot of chunk PM25 heatmap" alt="plot of chunk PM25 heatmap" style="display: block; margin: auto;" />

Similarly, 'pm25' also decreases along the period examined, and by 2013 only December shows significant high concentrations.

### Weather variables
Let's have a look at the weather variables, especially to see how they have evolved over time.

#### Wind speed and direction

```r
air_hour_avg = timeAverage(air, avg.time = "hour")

p1 = ggplot(air_complete, aes(x = vv)) + geom_histogram(binwidth = 1/10, colour = "white") + 
    ggtitle("All observations")

p2 = ggplot(air_complete, aes(x = vv)) + geom_histogram(binwidth = 1/10, colour = "white") + 
    coord_cartesian(xlim = c(0, quantile(air_complete$vv, 0.99))) + ggtitle("99 Percentile")

grid.arrange(p1, p2, ncol = 2, main = "Wind speed distribution")
```

<img src="figure/wind_speed_histogram.png" title="plot of chunk wind speed histogram" alt="plot of chunk wind speed histogram" style="display: block; margin: auto;" />

The wind speed distribution is also strongly right-skewed. The count after 3 is so low that it won't even show. A good way to inspect further wind speed is combining with wind direction using the openair function windRose. Looking at the histogram it seems wise to break the wind speed in intervals of 0.5 up to 3, and from there straight up to 17.1, which is the maximum value of the 'vv' variable.

```r
windRose(air_hour_avg, angle=45,
         wd='dd', ws="vv", # Wind direction and wind speed
         type="season",
         col='jet', # Colour palette
         breaks=c(0, 0.5, 1, 1.5, 2, 2.5, 3, 17.1)) # Breaks applied to the variable 'vv'
```

<img src="figure/windRose_seasons.png" title="plot of chunk windRose seasons" alt="plot of chunk windRose seasons" style="display: block; margin: auto;" />

summer and winter wind appear to be the most different, while spring and autumn seem transitions between them. During the summer months the most common wind blows from the north, from the sea since Gijon is a seaside city. This is quite common, as the temperature in the city air is higher than that above the air, the air rises up and it is replaced by the colder sea air. The northern wind is also the general trend during spring.
During winter, and autumn too, the wind directions appear equally divided between northern, southwest and southeast winds.

But has this situation being stable over the years?

```r
windRose(air_hour_avg, angle = 45, wd = "dd", ws = "vv", ws.int = 0.5, type = c("year", 
    "season"), col = "jet", breaks = c(0, 0.5, 1, 1.5, 2, 2.5, 3, 17.1))
```

<img src="figure/windRose_seasons_and_years.png" title="plot of chunk windRose seasons and years" alt="plot of chunk windRose seasons and years" style="display: block; margin: auto;" />

It seems that the wind seasonality over the years is more or less stable. But it is hard to tell from this graph. Let's plot them over time.

```r
p1 = ggplot(air_day_avg, aes(x = date, y = vv)) + geom_line(alpha = 0.5) + geom_smooth(se = FALSE) + 
    ggtitle("Wind speed daily average")

p2 = ggplot(air_week_avg, aes(x = date, y = vv)) + geom_line(alpha = 0.5) + 
    geom_smooth(se = FALSE) + ggtitle("Wind speed weekly average")

p3 = ggplot(air_month_avg, aes(x = date, y = vv)) + geom_line() + geom_smooth(se = FALSE) + 
    ggtitle("Wind speed monthly average")

grid.arrange(p1, p2, p3, ncol = 1)
```

```
## geom_smooth: method="auto" and size of largest group is >=1000, so using gam with formula: y ~ s(x, bs = "cs"). Use 'method = x' to change the smoothing method.
## geom_smooth: method="auto" and size of largest group is <1000, so using loess. Use 'method = x' to change the smoothing method.
## geom_smooth: method="auto" and size of largest group is <1000, so using loess. Use 'method = x' to change the smoothing method.
```

<img src="figure/Wind_speed_over_time.png" title="plot of chunk Wind speed over time" alt="plot of chunk Wind speed over time" style="display: block; margin: auto;" />

Although slightly, it seems that over the period 2009 - 2014, the average wind speed has declined. There seems to be also some seasonality in the behaviour of 'vv', so let's plot it against 'rs'.


```r
ggplot(air_month_avg, aes(x = date, y = vv)) + geom_line() + geom_line(aes(y = rs/120), 
    color = "red", linetype = 2)
```

<img src="figure/VV_over_time_against_RS.png" title="plot of chunk VV over time against RS" alt="plot of chunk VV over time against RS" style="display: block; margin: auto;" />

Dividing solar radiaton by 120 to overplot it on top of the wind speed series shows how they both evolve together. Also they both display a decreasing general trend. This clearly indicates that they are strongly related.


```r
ggplot(air_month_avg, aes(x = rs, y = vv)) + geom_point() + geom_smooth(method = lm, 
    se = TRUE)
```

<img src="figure/VV_by_RS_scatterplot.png" title="plot of chunk VV by RS scatterplot" alt="plot of chunk VV by RS scatterplot" style="display: block; margin: auto;" />

The scatterplot also shows that there is a relationship. How does this look on a linear model?

```r
summary(lm(vv ~ rs, data = air_month_avg))
```

```
## 
## Call:
## lm(formula = vv ~ rs, data = air_month_avg)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -0.2504 -0.1059 -0.0082  0.0759  0.3212 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)  0.45933    0.04661    9.86  5.3e-14 ***
## rs           0.00309    0.00038    8.13  3.8e-11 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 0.135 on 58 degrees of freedom
## Multiple R-squared:  0.532,	Adjusted R-squared:  0.524 
## F-statistic:   66 on 1 and 58 DF,  p-value: 3.78e-11
```

It is quite remarkable that using just a single variable, 'rs', we can predict up to 53% of variability in 'vv'.


Finally, to get a different perspective on the evolution of the wind speed, let's have a look at a heatmap.

```r
trendLevel(air, "vv", layout = c(5, 1))
```

<img src="figure/wind_speed_heatmap.png" title="plot of chunk wind speed heatmap" alt="plot of chunk wind speed heatmap" style="display: block; margin: auto;" />

Over a 24-hour period it is clear that the wind is stronger during the day, 10:00 to 18:00 or 19:00. Also, as we saw in the time series, the wind blows stronger during the summer months and the levels decrease during autumn and winter.

It is also possible to look at a heatmap relationship of wind speed, 'vv', by wind direction, 'dd'.

```r
# In this case we will have to use a format friendlier to the openair
# function create a new data frame and rename the variables 'dd' and 'vv' to
# 'wd' and 'ws'
air_openair <- air
names(air_openair)[12] = "wd"
names(air_openair)[13] = "ws"

trendLevel(air_openair, pollutant = "ws", y = "wd", layout = c(5, 1))
```

```
## Warning: 413 missing wind direction line(s) removed
```

<img src="figure/wind_speed_by_heatmap.png" title="plot of chunk wind speed by heatmap" alt="plot of chunk wind speed by heatmap" style="display: block; margin: auto;" />

From this perspective, it seems that over the years, the stronger wind blows from the north and northeast, but it is harder to interpret the overall trend using this visualization.

#### Relative humidity

```r
p1 = ggplot(air_week_avg, aes(x = date, y = hr)) + geom_line(alpha = 0.5) + 
    geom_smooth(se = FALSE) + ggtitle("Relative humidity weekly mean")

p2 = ggplot(air_month_avg, aes(x = date, y = hr)) + geom_line() + geom_smooth(se = FALSE) + 
    ggtitle("Relative humidity monthly mean")

grid.arrange(p1, p2, ncol = 1)
```

```
## geom_smooth: method="auto" and size of largest group is <1000, so using loess. Use 'method = x' to change the smoothing method.
## geom_smooth: method="auto" and size of largest group is <1000, so using loess. Use 'method = x' to change the smoothing method.
```

<img src="figure/HR_time_series.png" title="plot of chunk HR time series" alt="plot of chunk HR time series" style="display: block; margin: auto;" />

There is no special seasonality in the amount of 'hr', although it is interesting that the mean values climbed steadily during the period 2009 - 2012, and decreased rapidly in 2013.

Let's see it as a heatmap.

```r
trendLevel(air_openair, "hr", layout = c(5, 1))
```

<img src="figure/HR_heatmap.png" title="plot of chunk HR heatmap" alt="plot of chunk HR heatmap" style="display: block; margin: auto;" />

The heatmap shows from another perspective the same trend displayed in the time series, 'hr' increased over the period 2009 - 2012, and decreased rapidly in 2013. It also seems that the 'hr' levels are always lower around noon and rise strongly during the night towards the early hours of the day, right before 08:00.

#### Rain
What was the evolution of the levels of rain in view of the high levels of relative humidity?

```r
p1 = ggplot(air_week_avg, aes(x = date, y = ll)) + geom_line(alpha = 0.5) + 
    geom_smooth(se = FALSE) + ggtitle("Rain weekly mean")

p2 = ggplot(air_month_avg, aes(x = date, y = ll)) + geom_line() + geom_smooth(se = FALSE) + 
    ggtitle("Rain monthly mean")

grid.arrange(p1, p2, ncol = 1)
```

```
## geom_smooth: method="auto" and size of largest group is <1000, so using loess. Use 'method = x' to change the smoothing method.
## geom_smooth: method="auto" and size of largest group is <1000, so using loess. Use 'method = x' to change the smoothing method.
```

<img src="figure/LL_time_series.png" title="plot of chunk LL time series" alt="plot of chunk LL time series" style="display: block; margin: auto;" />

Interestingly, although the 'hr' levels kept rising for some years, it doesn't seem that the 'll' values followed it along.


```r
trendLevel(air, "ll", layout = c(5, 1))
```

<img src="figure/LL_heatmap.png" title="plot of chunk LL heatmap" alt="plot of chunk LL heatmap" style="display: block; margin: auto;" />

It is clear that the rain levels were higher in the fall of 2010, April 2012 and finally on the fall of 2012 and winter of 2013. They remained quite low otherwise, close to zero in most of cases. This is interesting as it was clear that relative humidity was steadily high for a couple of years. The higher rain mean in the winter of 2013 coincides with the final decrease of relative humidity in the same period.

#### Atmospheric pressure
Atmospheric is a variable strongly related with 'll', rain, how has it evolved?

```r
p1 = ggplot(air_week_avg, aes(x = date, y = prb)) + geom_line(alpha = 0.5) + 
    geom_smooth(se = FALSE) + ggtitle("Atmospheric pressure weekly mean")

p2 = ggplot(air_month_avg, aes(x = date, y = prb)) + geom_line() + geom_smooth(se = FALSE) + 
    ggtitle("Atmospheric pressure monthly mean")

grid.arrange(p1, p2, ncol = 1)
```

```
## geom_smooth: method="auto" and size of largest group is <1000, so using loess. Use 'method = x' to change the smoothing method.
## geom_smooth: method="auto" and size of largest group is <1000, so using loess. Use 'method = x' to change the smoothing method.
```

<img src="figure/PRB_time_series.png" title="plot of chunk PRB time series" alt="plot of chunk PRB time series" style="display: block; margin: auto;" />

'prb' does not display a seasonal trend, but it seems that on average it has raised over time, which makes sense taking into account that the levels of rain were low during a couple of years. The dip in the winter of 2013 coincides with the increase in rain and the decrease in relative humidity.


```r
trendLevel(air, "prb", layout = c(5, 1))
```

<img src="figure/PRB_heatmap.png" title="plot of chunk PRB heatmap" alt="plot of chunk PRB heatmap" style="display: block; margin: auto;" />

The heatmap confirms how the atmospheric pressure, 'prb', has increased over time, and shows that the winter months of 2012 registered the highest values.

#### Solar radiation
We have already checked the trend of 'rs', but let's review it once more.

```r
p1 = ggplot(air_week_avg, aes(x = date, y = rs)) + geom_line(alpha = 0.5) + 
    geom_smooth(se = FALSE) + ggtitle("Solar radiation weekly mean")

p2 = ggplot(air_month_avg, aes(x = date, y = rs)) + geom_line() + geom_smooth(se = FALSE) + 
    ggtitle("Solar radiation monthly mean")

grid.arrange(p1, p2, ncol = 1)
```

```
## geom_smooth: method="auto" and size of largest group is <1000, so using loess. Use 'method = x' to change the smoothing method.
## geom_smooth: method="auto" and size of largest group is <1000, so using loess. Use 'method = x' to change the smoothing method.
```

<img src="figure/RS_time_series.png" title="plot of chunk RS time series" alt="plot of chunk RS time series" style="display: block; margin: auto;" />

'rs' does display a strong seasonal behaviour and it has also steadily decreased over time.

```r
trendLevel(air, "rs", layout = c(5, 1))
```

<img src="figure/RS_heatmap.png" title="plot of chunk RS heatmap" alt="plot of chunk RS heatmap" style="display: block; margin: auto;" />

The heatmap reflects quite nicely the evolution of 'rs' over a 24-hour range by month and year. It is easy to see how it depicts perfectly the difference between day and night. The sun shines longer and 'stonger' during the summer. May 2011 and April 2012 show lower values of 'rs', possibly due to exceptional nubosity. Actually, looking back at 'll' values during April 2012, it does seem that it was caused by rain, and therefore nubosity. This behaviour, as we saw, is mirrored by the levels of 'vv'.

#### Temperature

```r
p1 = ggplot(air, aes(x = date, y = tmp)) + geom_line() + geom_smooth(method = lm, 
    se = TRUE) + ggtitle("Hourly observations")

p2 = ggplot(air_day_avg, aes(x = date, y = tmp)) + geom_line() + geom_smooth(method = lm, 
    se = TRUE) + ggtitle("Daily mean")

p3 = ggplot(air_week_avg, aes(x = date, y = tmp)) + geom_line(alpha = 0.5) + 
    geom_smooth(se = FALSE) + ggtitle("Temperature weekly mean")

p4 = ggplot(air_month_avg, aes(x = date, y = tmp)) + geom_line() + geom_smooth(se = FALSE) + 
    ggtitle("Temperature monthly mean")

grid.arrange(p1, p2, p3, p4, ncol = 1)
```

```
## Warning: Removed 425 rows containing missing values (stat_smooth).
```

```
## geom_smooth: method="auto" and size of largest group is <1000, so using loess. Use 'method = x' to change the smoothing method.
## geom_smooth: method="auto" and size of largest group is <1000, so using loess. Use 'method = x' to change the smoothing method.
```

<img src="figure/TMP_time_series.png" title="plot of chunk TMP time series" alt="plot of chunk TMP time series" style="display: block; margin: auto;" />

After seeing how the overall values of solar radiation declining over time, it is interesting to see that the temperature, 'tmp', does not follow the same pattern, and it has actually remained quite constant over the period 2009 - 2013. Let's see them together.

```r
ggplot(air_month_avg, aes(x = date, y = tmp)) + geom_line() + geom_line(aes(y = rs/10), 
    color = "red", linetype = 2)
```

<img src="figure/Time_series_TMP_and_RS.png" title="plot of chunk Time series TMP and RS" alt="plot of chunk Time series TMP and RS" style="display: block; margin: auto;" />

It is clear that they are related as their pattern oscillates throughout the year. We can also see how there is a slight delay in temperature as the solar radiation increases, but at the end they both reach the highest value every year pretty much at the same time. However, some information is missing, as we can see how solar radiation declines but temperature does not.


```r
ggplot(air_month_avg, aes(x = rs, y = tmp)) + geom_point() + geom_smooth(method = lm, 
    se = TRUE)
```

<img src="figure/Scatterplot_TMP_and_RS.png" title="plot of chunk Scatterplot TMP and RS" alt="plot of chunk Scatterplot TMP and RS" style="display: block; margin: auto;" />

The same idea is confirmed in the scatterplot. They both evolve together but solar radiation alone does not explain the changes the in temperature as there is a lot of scatter.



```r
trendLevel(air, "tmp", layout = c(5, 1))
```

<img src="figure/TMP_heatmap.png" title="plot of chunk TMP heatmap" alt="plot of chunk TMP heatmap" style="display: block; margin: auto;" />

The 'tmp' heatmap also displays nicely how the temperature varies during the day and night across months and seasons. The higher 'tmp' values are registered around 14:00. It is also clear how there is a strong temperature difference during the winter months from day to night, and how during the summer months high temperatures do not dissipate easily during the night.
On a year to year basis, it seems that the summer of 2009 was much hotter than the rest, and also summer and autumn of 2013.


## Final Plots and summary

### Final plot one

```r
ggplot(air_year_avg, aes(x = date, y = pm25)) + geom_point(size = 4) + geom_line(aes(y = pm25), 
    linetype = 1, size = 0.25) + geom_hline(yintercept = 10, color = "red", 
    linetype = 2) + coord_cartesian(ylim = c(0, 15)) + xlab("Date") + ylab("micrograms / cubic meter") + 
    ggtitle("PM25 annual mean and WHO suggested limit")
```

<img src="figure/Final_plot_1.png" title="plot of chunk Final plot 1" alt="plot of chunk Final plot 1" style="display: block; margin: auto;" />

From 2009 to 2013, the annual average levels of particulate matter smaller than or equal to 2.5 micrometers have been always above the levels recommended by the WHO. Particulate matter affects more people than any other pollutant. Small particulate matter can form when gas emissions from different sources react in the air and their health effects can be very serious, as such small particles can penetrate into the lung and even into the bloodstream


### Final plot two

```r
trendLevel(air, "no2", layout = c(5, 1), main = "Nitrogen dioxide levels over time")
```

<img src="figure/Final_plot_2.png" title="plot of chunk Final plot 2" alt="plot of chunk Final plot 2" style="display: block; margin: auto;" />

The main source of NO2 emissions are combustion processes, such as heating, power generation and internal combustion engines. The plot shows clearly how the levels of NO2 raise during autumn and winter and decrease during spring and summer. This seems strongly related with temperature variability along the year. It is also visible how throughout the year the highest concentrations occur at rush hours, 08:00 to 09:00 in the morning and around 19:00 in the evening, which suggests that there is a strong human factor in its production. The WHO warns that long exposure to NO2 is related with cases of bronchitis in asthmatic children and reduced lung function growth. Additionally, NO2 is a lso a source of fine particulate matter, which is a very dangerous pollutant.


### Final plot three

```r
ggplot(air_month_avg, aes(x = date, y = vv)) + geom_line() + geom_line(aes(y = rs/120), 
    colour = "red", linetype = 2) + ylab("Meters / second") + xlab("Date") + 
    ggtitle("Wind speed and solar radiation")
```

<img src="figure/Final_plot_3.png" title="plot of chunk Final plot 3" alt="plot of chunk Final plot 3" style="display: block; margin: auto;" />

One of the most interesting weather patterns I noticed was how closely related solar radiation and wind speeds are. To have a better view I have overplotted solar radiation in red, and scaled it to lie it on top of the wind speed series. Their general trend even declines over time together. Actually a linear regression model shows that solar radiation alone explains up to 53% of the behaviour of wind speeds.

## Reflection
Finding and cleaning a data set has been the most difficult part, even though the data was already in good shape. I found data available for the period 2000 to 2014, however, not all the variables were measured equally well, some weren't measured at all for some years, and there were plenty of variables too. I did not know on which variables to focus or why. I decided to look for some guidelines about air pollution at the World Health Organization website and found that they especially focus on five pollutants, so I decided to follow them too. pm25 had not been measured for the period 2000 to 2008, so I decided to cut those years out, there were still a good amount of observations. Taking these decisions and following different approaches only to find lots of dead ends took me a fair few days.

I did not know what I was going to find about the interplay of pollutants and weather variables. My first attempt, in retrospective, was rather chaotic. It was especially frustrating to see that there was not a clear relationship between any of them. Now, on my second attempt, I can clearly see that I rushed over the data too quickly. On the second time I went carefully inspecting the variables one by one and considering all possible relations and approaches that came to mind. I also looked for information on every variable analysed, and using mostly the guidelines provided by the World Health Organization and the American Environmental Protection Agency I was able to take every analysis a little bit further. I was even able to build some interesting simple linear regression models.

Future exploration of the data could focus on fewer pollutants, perhaps just one, and analyze it very carefully. There are other four weather stations in the city for which there is available data too. Although not all of them provide the same level of detail (most of them show far fewer observations than the station chosen for this study) so it would be possible to carry out a comparison study about the air quality in different areas of the city too.

Lastly, The World Health Organization provides guidelines and limits that could be used to provide  management tools for the air quality in the city. It would also be interesting to analyse them in combination with health care studies focusing on the conditions caused by every pollutant.
